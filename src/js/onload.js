import wrapPromise from "./wrapPromise.js";

function onLoad(onLoaded, waitForClient) {
  if (waitForClient) {
    wrapPromise(function() {
      onLoad(onLoaded, false);
    });
  } else {
    if (
      document.readyState == "interactive" ||
      document.readyState == "complete"
    )
      onLoaded();
    else window.addEventListener("DOMContentLoaded", onLoaded);
  }
}

export default onLoad;
