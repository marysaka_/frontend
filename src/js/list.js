import "intersection-observer";
import "@/styles/list.scss";
import superagent from "superagent";
import stubbedImage from "@/icons/loading.svg";
import path from "path";
import copyImage from "@/icons/copy.svg";
import openImage from "@/icons/open.svg";
import Clipboard from "clipboard";
import deleteImage from "@/icons/delete.svg";
import commonCode from "./commonCode.js";
import filesize from "file-size";

let loadedAll = false;

let wantsToDelete = null;

// Just going to override the mimes pillow doesn't support
const overrideMimes = [
  "image/svg+xml",
  "application/svg+xml",
  "application/svg",
  "image/svg"
];
window.addEventListener("load", async function() {
  const fileGrid = document.getElementById("file-grid");
  let pageNum = 0;
  const modalShowBtn = document.getElementById("show-delete-modal");
  const deleteAlerts = {};
  const fileContainers = {};
  const loadedFiles = [];
  await loadMore(0);
  async function loadMore(pageNum = 0) {
    const frag = document.createDocumentFragment();
    const { files } = await client.getFiles(pageNum);
    const fileList = [];
    for (const shortname in files) {
      if (!loadedFiles.includes(shortname)) {
        // Dedupe stuff because stuff on the page can change causing dupes
        fileList.push(files[shortname]);
        loadedFiles.push(shortname);
      }
    }
    if (fileList.length === 0) {
      return (loadedAll = true);
    }
    const fileListSorted = fileList.sort(
      (b, a) => Number(a.snowflake) - Number(b.snowflake)
    );
    for (const file of fileListSorted) {
      fileContainers[file.shortname] = renderFile(file);
      frag.appendChild(fileContainers[file.shortname]);
    }
    fileGrid.appendChild(frag);
  }

  window.addEventListener("scroll", ev => {
    if (
      window.innerHeight + window.scrollY >= document.body.offsetHeight &&
      !loadedAll
    ) {
      // got the bottom of the page, load some more
      loadMore(++pageNum);
    }
  });

  const deleteConfirm = document.getElementById("delete-confirm");
  deleteConfirm.addEventListener("click", function() {
    actuallyDelete();
  });

  async function actuallyDelete() {
    const toDelete = wantsToDelete;
    wantsToDelete = null;
    if (deleteAlerts[toDelete]) {
      removeAlert(deleteAlerts[toDelete]);
      deleteAlerts[toDelete] = null;
    }
    try {
      await client.deleteFile(toDelete);
      fileContainers[toDelete].remove();
      delete fileContainers[toDelete];
    } catch (err) {
      if (err.errorCode == "NOT_FOUND") {
        deleteAlerts[toDelete] = commonCode.sendAlert(
          "warning",
          err.userMessage
        );
        return fileContainer.remove();
      }
      deleteAlerts[toDelete] = commonCode.sendAlert(
        "danger",
        err.userMessage || `An unknown error occurred: ${err.message}`
      );
      throw err;
    }
  }

  function renderFile(file) {
    const fileContainer = document.createElement("div");
    fileContainer.classList =
      "file-container col-12 col-sm-12 col-md-6 col-lg-4";
    const fileWrap = document.createElement("div");
    fileWrap.classList = "file-wrap";
    const previewContainer = document.createElement("div");
    previewContainer.classList = "preview-container";
    let previewTransport = document.createElement("img");
    previewTransport.classList = "stubbed-preview preview-transport";
    previewTransport.src = stubbedImage;
    // Done playing by the rules tbh
    previewContainer.attributes["data-url"] = overrideMimes.includes(
      file.mimetype
    )
      ? file.url
      : file.thumbnail;
    previewContainer.attributes["data-size"] = file.size;
    previewContainer.attributes["data-mimetype"] = file.mimetype;

    const fileSize = document.createElement("div");
    fileSize.innerText = filesize(file.size).human();
    fileSize.classList = "file-size text-muted";

    const iconRow = document.createElement("div");
    const bottomRow = document.createElement("div");
    const deleteBtn = document.createElement("a");
    const copyBtn = document.createElement("a");
    const openBtn = document.createElement("a");
    const copyImg = document.createElement("img");
    const openImg = document.createElement("img");
    const deleteImg = document.createElement("img");
    openImg.src = openImage;
    copyImg.src = copyImage;
    copyBtn.href = "#";
    deleteImg.src = deleteImage;
    openBtn.appendChild(openImg);

    copyBtn.appendChild(copyImg);
    openBtn.classList = "vector-btn";
    copyBtn.classList = "vector-btn";
    openBtn.href = file.url;
    openBtn.target = "_blank";

    const clipboard = new Clipboard(copyBtn, {
      text: function() {
        return openBtn.href;
      }
    });
    clipboard.on("success", function(ev) {
      const alertId = commonCode.sendAlert("success", "Copied to clipboard!");
      setTimeout(() => commonCode.removeAlert(alertId), 1500);
    });
    copyBtn.addEventListener("click", function(ev) {
      ev.preventDefault();
    });

    bottomRow.classList = "bottom-row";
    iconRow.classList = "icon-row";
    deleteBtn.classList = "vector-btn";
    deleteBtn.appendChild(deleteImg);
    deleteBtn.href = "#";
    deleteBtn.addEventListener("click", function(ev) {
      ev.preventDefault();
      wantsToDelete = file.shortname;
      modalShowBtn.click();
    });

    iconRow.appendChild(copyBtn);
    iconRow.appendChild(deleteBtn);
    iconRow.appendChild(openBtn);
    bottomRow.appendChild(iconRow);

    previewContainer.appendChild(previewTransport);
    previewTransport.addEventListener("load", function() {
      observer.observe(previewContainer);
      if (isVisible(previewContainer)) {
        console.log("Render!");
        renderRealPreview(
          overrideMimes.includes(file.mimetype) ? file.url : file.thumbnail,
          previewContainer,
          file.size,
          file.mimetype
        );
      }
    });
    fileWrap.appendChild(previewContainer);
    fileWrap.appendChild(fileSize);
    fileWrap.appendChild(bottomRow);

    fileContainer.appendChild(fileWrap);
    return fileContainer;
  }
});

const observer = new IntersectionObserver(
  function(entries) {
    for (const entry of entries) {
      if (
        !entry.isIntersecting ||
        entry.target.attributes["data-loaded-preview"] == "true"
      )
        return;
      const fileUrl = entry.target.attributes["data-url"];
      const mimetype = entry.target.attributes["data-mimetype"];
      renderRealPreview(
        fileUrl,
        entry.target,
        entry.target.attributes["data-size"],
        mimetype
      );
    }
  },
  {
    rootMargin: "60px"
  }
);

async function renderRealPreview(fileUrl, previewContainer, size, mimetype) {
  previewContainer.attributes["data-loaded-preview"] = "true";
  let loadingBlock = null;
  if (
    mimetype.startsWith("image/") ||
    mimetype == "application/svg+xml" ||
    mimetype.startsWith("audio/") ||
    mimetype.startsWith("video/")
  ) {
    const req = superagent.get(fileUrl).responseType("blob");
    req.on("progress", function(prog) {
      progressBar.style.width = `${prog.percent || 0}%`;
    });
    req.catch(err => console.warn(err));
    const progressBarWrap = document.createElement("div");
    loadingBlock = document.createElement("div");
    loadingBlock.classList = "loading-block";
    progressBarWrap.classList = "progress";
    const progressBar = document.createElement("div");
    progressBar.classList =
      "progress-bar progress-bar-striped progress-bar-animated bg-primary";

    progressBarWrap.appendChild(progressBar);
    loadingBlock.appendChild(progressBarWrap);
    previewContainer.innerHTML = "";
    previewContainer.appendChild(loadingBlock);
    try {
      var res = await req;
    } catch (err) {}
  } else {
    previewContainer.innerHTML = "";
  }

  function finishLoading() {
    if (loadingBlock) loadingBlock.remove();
  }

  const blobUrl = (res && URL.createObjectURL(res.body)) || fileUrl;
  let previewTransport = null;
  console.log("Ready to render!", mimetype, previewTransport, blobUrl);
  if (mimetype.startsWith("video/")) {
    previewTransport = document.createElement("video");
    previewTransport.controls = true;
    const previewSource = document.createElement("source");
    previewSource.type = mimetype;
    previewSource.src = blobUrl;
    previewTransport.appendChild(previewSource);
    previewTransport.addEventListener("canplaythrough", function() {
      finishLoading();
      previewContainer.appendChild(previewTransport);
    });
  } else if (mimetype.startsWith("audio/")) {
    previewTransport = document.createElement("div");
    const previewAudio = document.createElement("audio");
    const previewSource = document.createElement("source");
    previewAudio.controls = true;
    previewSource.type = mimetype;
    previewSource.src = blobUrl;
    previewAudio.appendChild(previewSource);
    previewTransport.appendChild(previewAudio);
    previewAudio.addEventListener("canplaythrough", function() {
      finishLoading();
      previewContainer.appendChild(previewTransport);
    });
  } else if (
    mimetype.startsWith("image/") ||
    mimetype == "application/svg+xml"
  ) {
    previewTransport = document.createElement("img");
    previewTransport.src = blobUrl;
    previewTransport.addEventListener("load", function() {
      finishLoading();
      URL.revokeObjectURL(blobUrl);
      previewContainer.appendChild(previewTransport);
    });
  } else {
    // TODO: Maybe get backend to send us mimes on list endpoint so we don't waste a request here?
    previewTransport = document.createElement("div");
    previewTransport.innerText = `Cannot display a preview for this files of this type: ${mimetype}`;
    previewTransport.classList += " unknown-file-preview";
    finishLoading();
    previewContainer.appendChild(previewTransport);
    URL.revokeObjectURL(blobUrl);
  }
  previewTransport.classList += " preview-transport";
}

function isVisible(elem) {
  const rect = elem.getBoundingClientRect();
  const windowHeight =
    window.innerHeight || document.documentElement.clientHeight;
  const windowWidth = window.innerWidth || document.documentElement.clientWidth;

  return (
    rect.top <= windowHeight &&
    rect.top + rect.height >= 0 &&
    rect.left <= windowWidth &&
    rect.left + rect.width >= 0
  );
}
