# Elixire Frontend
Frontend for [elixire](https://gitlab.com/elixire/elixire) made using Bootstrap and Webpack.

## Installing
Installing should be as easy as just cloning the repo and grabbing the deps
```bash
git clone git@gitlab.com:elixire/frontend.git
yarn install
```

## Building
Building is also fairly easy, just run
```bash
yarn build:production
```
Or if you want an unminified build for testing
```bash
yarn build:dev
```
You can also use [webpack-dev-server](https://github.com/webpack-community/webpack-dev-server) to test changes live
```bash
yarn start
```
